A set of simple classes and style files to avoid tome boilerplate LaTeX code.

All the classes and style files are documented in ``templates.pdf``.

If you're using [latexbuild](https://gitlab.com/llvilanova/latexbuild), it is
more convenient to get a copy of ``Makefile.sample`` here to build your LaTeX
documents using ``make``:

<pre>
curl https://gitlab.com/llvilanova/latex-templates/raw/master/Makefile.sample > Makefile
make
</pre>

